﻿using System;
using System.Drawing;

public class VectorGame
{

    public MyVector[] randomVectors(int length)
    {
        MyVector[] arrayVector = new MyVector[length];
        Random random = new Random();

        for (int i = 0; i < length; i++)
        {
            arrayVector[i] = new MyVector(new Point(random.Next(150)-1, random.Next(150)- 1), new Point(random.Next(150)- 1, random.Next(150)- 1));
        }

        return arrayVector;
    }

}
