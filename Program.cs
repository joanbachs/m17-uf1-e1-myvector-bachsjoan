﻿using System;

namespace M17_UF1_E1_MyVector_BachsJoan
{
    class Program
    {

        static void Main(string[] args)
        {
            Hello();

            MyVector vector = new MyVector(34, 23, 43, 6);
            vector.getPuntInicial();
            vector.getPuntFinal();
            vector.setPuntInici(3, 9);
            vector.setPuntFinal(6, 4);
            Console.WriteLine(vector.distanciaVector());
            vector.cambiarSentitVector();
            vector.ToString();

            VectorGame randomVector = new VectorGame();
            randomVector.randomVectors(8);
        }

        private static void Hello()
        {
            Console.WriteLine("Escriu el teu nom");
            string nom = Console.ReadLine();
            Console.WriteLine("Hello " + nom);
        }

    }
}
