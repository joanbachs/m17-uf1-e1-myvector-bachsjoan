﻿using System;
using System.Drawing;

public class MyVector
{
	public double [] puntI = new double [2];
	public double [] puntF = new double [2];
    public double puntIx;
    public double puntIy;
    public double puntFx;
    public double puntFy;
    private Point point1;
    private Point point2;

    public void setPuntInici(double x, double y)
    {
        puntIx = x;
        Console.WriteLine("La coordenada X0 és: " + puntIx);
        puntIy = y;
        Console.WriteLine("La coordenada Y0 és: " + puntIy);

    }

    public void setPuntFinal(double x, double y)
    {
        puntFx = x;
        Console.WriteLine("La coordenada X1 és: " + puntFx);
        puntFy = y;
        Console.WriteLine("La coordenada Y1 és: " + puntFy);
    }

    public double[] getPuntInicial()
    {
        puntI[0] = puntIx;
        Console.WriteLine("La coordenada X0 és: " + puntIx);
        puntI[1] = puntIy;
        Console.WriteLine("La coordenada Y0 és: " + puntIy);
        return puntI;
    }
    public double[] getPuntFinal()
    {
        puntF[0] = puntFx;
        Console.WriteLine("La coordenada X1 és: " + puntFx);
        puntF[1] = puntFy;
        Console.WriteLine("La coordenada Y1 és: " + puntFy);
        return puntF;
    }

    public double distanciaVector()
    {

        return Math.Sqrt(Math.Pow((puntIy - puntIx), 2) + Math.Pow((puntFy - puntFx), 2));
        /*
        double pow = puntIy - puntIx;
        double res = Math.Pow(pow, 2);
        double distancia = Math.Sqrt(res);
        return distancia;
        */
    }


    public void cambiarSentitVector()
    {
        puntI[0] = puntFx;
        puntI[1] = puntFy;
        puntF[0] = puntIx;
        puntF[1] = puntIy;

        Console.WriteLine("Vector cambiat de sentit. Ara el vector és: ");
        imprimirVector();

    }

    public void imprimirVector()
    {
        Console.WriteLine("Punt inicial: "+puntI[0] +", " + puntI[1]);
        Console.WriteLine("Punt final: " + puntF[0] + ", " + puntF[1]);


    }

    public override string ToString()
    {
        return "Punt inicial: " + puntI[0] + ", " + puntI[1] + " i el punt final: " + puntF[0] + ", " + puntF[1];
    }

    public MyVector(double pIx, double pIy, double pFx, double pFy)
	{
        this.puntIx = pIx;
        this.puntIy = pIy;
        this.puntFx = pFx;
        this.puntFy = pFx;
    }

    public MyVector(Point point1, Point point2)
    {
        this.point1 = point1;
        this.point2 = point2;
    }
}
